﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dot : MonoBehaviour {


    public int color;
    public bool isMatched;
    public int x;
    public int y;
    public GameObject particles;
    public ParticleSystem.MainModule ps;
    public SpriteRenderer sr;
    // Use this for initialization
    void Start ()
    {
        ps = particles.GetComponent<ParticleSystem>().main;
        sr = this.GetComponent<SpriteRenderer>();
		switch(color)
        {
            case 0:
                sr.color = new Color(1,0,0);
                ps.startColor = sr.color;
                break;
            case 1:
                sr.color = new Color(1, 121 / 255f, 0);
                ps.startColor = sr.color;
                break;
            case 2:
                sr.color = new Color(1, 1, 0);
                ps.startColor = sr.color;
                break;
            case 3:
                sr.color = new Color(57 / 255f , 1, 0);
                ps.startColor = sr.color;
                break;
            case 4:
                sr.color = new Color(0, 1, 1);
                ps.startColor = sr.color;
                break;
            case 5:
                sr.color = new Color(.25f, .25f, 1);
                ps.startColor = sr.color;
                break;
            case 6:
                sr.color = new Color(123 / 255f, 0, 1);
                ps.startColor = sr.color;
                break;
            case 7:
                sr.color = new Color(1, 0, 1);
                ps.startColor = sr.color;
                break;

        }
	}
	

	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        if (GameManager.instance.timer > 0)
        {
            particles.SetActive(true);
            GameManager.instance.currentColor = sr.color;
            isMatched = true;
            GameManager.instance.currentActivePoints++;
            GameManager.instance.firstClickedDot = this.gameObject;
            GameManager.instance.mostRecentDot = this.gameObject;
            GameManager.instance.currentColorInt = color;
            GameObject temp = Instantiate(GameManager.instance.linePrefab, this.gameObject.transform.position, Quaternion.identity) as GameObject;
            GameManager.instance.lines.Add(temp);
            GameManager.instance.currentLine = temp;
        }
        
    }

    private void OnMouseOver()
    {
        if (GameManager.instance.timer > 0)
        {
            if (color == GameManager.instance.currentColorInt && GameManager.instance.mouseIsDown == true)
            {
                if (Vector3.Distance(GameManager.instance.mostRecentDot.transform.position, this.transform.position) <= GameManager.instance.spacer * 1.5f)
                {
                    particles.SetActive(true);
                    if (isMatched == false)
                    {
                        particles.gameObject.transform.localScale = new Vector3(1, 1, 1) * (1 + (GameManager.instance.currentActivePoints * 0.4f));
                        GameManager.instance.currentLine.transform.localScale = new Vector3(Vector3.Distance(GameManager.instance.currentLine.transform.position, this.gameObject.transform.position), 0.15f, 1);
                        GameManager.instance.currentLine.transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2((this.gameObject.transform.position.y - GameManager.instance.currentLine.transform.position.y), (this.gameObject.transform.position.x - GameManager.instance.currentLine.transform.position.x)) * Mathf.Rad2Deg);


                        isMatched = true;
                        GameManager.instance.currentActivePoints++;
                        GameManager.instance.mostRecentDot = this.gameObject;
                        GameObject temp = Instantiate(GameManager.instance.linePrefab, this.gameObject.transform.position, Quaternion.identity) as GameObject;
                        GameManager.instance.lines.Add(temp);
                        GameManager.instance.currentLine = temp;
                    }

                }
            }
        }

            
    }
}
