﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{

    public static GameManager instance = null;
    public int differentColors;
    public Color32 currentColor;
    public int currentColorInt;
    public int sizeX;
    public int sizeY;
    public float spawnX;
    public float spawnY;
    public float spacer;
    public GameObject[,] dots;
    public List<GameObject> lines = new List<GameObject>();
    public GameObject dotPrefab;
    public GameObject linePrefab;
    public bool isResolving = false;
    public bool allMovedDown = true;
    public int currentActivePoints;
    public bool mouseIsDown = false;
    public float score;
    public float timer = 60.0f;
    public Text scoreText;
    public Text timerText;
    public GameObject firstClickedDot;
    public GameObject mostRecentDot;

    public GameObject currentLine;

    public GameObject quitButton;
    public GameObject restartButton;
    void Awake()
    {
        //DontDestroyOnLoad(quitButton); 
        //DontDestroyOnLoad(restartButton);
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
           // DontDestroyOnLoad(gameObject);
            instance = this;
        }
    }
    // Use this for initialization
    void Start()
    {
        dots = new GameObject[sizeX, sizeY];
        CreateDots();
    }

    // Update is called once per frame
    void Update()
    {
        if(timer > 0)
        {
            timer -= Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                CreateDots();
            }
        }
        if (timer <= 0)
        {
            timer = 0;
            restartButton.SetActive(true);
            quitButton.SetActive(true);
        }

        if (Input.GetMouseButtonDown(0))
        {
            mouseIsDown = true;
        }

        if (Input.GetMouseButtonUp (0))
        {
            mouseIsDown = false;
            //run the lift finger code
            fingerLift();

        }

        if (currentLine != null)
        {
            Vector3 mouseLocation = Input.mousePosition;
            mouseLocation.z = 10;
            mouseLocation = Camera.main.ScreenToWorldPoint(mouseLocation);
            currentLine.GetComponent<SpriteRenderer>().color = currentColor;
            currentLine.transform.localScale = new Vector3(Vector3.Distance(currentLine.transform.position, mouseLocation), 0.15f, 1);
            currentLine.transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2((mouseLocation.y - currentLine.transform.position.y), (mouseLocation.x - currentLine.transform.position.x)) * Mathf.Rad2Deg);

        }

        scoreText.text = "Score: " + score.ToString("F0");
        timerText.text = "Timer: " + timer.ToString("F2");
    }

    public void Restart()
    {

        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
    public void Quit()
    {
        Application.Quit();
    }
    static public float BounceEaseOut(float p)
    {
        if (p < 4 / 11.0f)
        {
            return (121 * p * p) / 16.0f;
        }
        else if (p < 8 / 11.0f)
        {
            return (363 / 40.0f * p * p) - (99 / 10.0f * p) + 17 / 5.0f;
        }
        else if (p < 9 / 10.0f)
        {
            return (4356 / 361.0f * p * p) - (35442 / 1805.0f * p) + 16061 / 1805.0f;
        }
        else
        {
            return (54 / 5.0f * p * p) - (513 / 25.0f * p) + 268 / 25.0f;
        }
    }

    public IEnumerator BallBounce(GameObject fallingDot, Vector3 startPoint, Vector3 destinationPoint, float time, float speed)
    {
        for (float i = 0; i < time; i += Time.deltaTime * speed)
        {
            fallingDot.GetComponent<Transform>().position = Vector3.Lerp(startPoint, destinationPoint, BounceEaseOut(time * i));
            yield return new WaitForSeconds(Time.deltaTime);
        }
        fallingDot.transform.position = destinationPoint;
    }

    public IEnumerator CheckDotsIEnumerator()
    {
        allMovedDown = false;
        isResolving = true;
        for (int i = 0; i < sizeY - 1; i++)
        {
            //if on our last trip we didnt see that everything is already all the way down
            if (allMovedDown == false)
            {
                CheckDots();
                yield return new WaitForSeconds(0.1f);
            }
           
        }
        CreateDots();
        isResolving = false;
    }

    public void fingerLift()
    {
        if(currentActivePoints >= 2)
        {
            //For every collumn
            for (int x = 0; x < sizeX; x++)
            {
                //In every row
                for (int y = 0; y < sizeY; y++)
                {
                    if (dots[x, y].GetComponent<Dot>().isMatched == true)
                    {
                        //destroy that dot and set its location in the array to null
                        Destroy(dots[x, y]);
                        dots[x, y] = null;

                    }
                }
            }

            //give score
            score += 10 * (Mathf.Pow(currentActivePoints, 1.5f));
            
            //run check dots
            StartCoroutine(CheckDotsIEnumerator());
        }
        if (currentActivePoints < 2)
        {
            //we didnt match so reset the dots
            //For every collumn
            for (int x = 0; x < sizeX; x++)
            {
                //In every row
                for (int y = 0; y < sizeY; y++)
                {
                    if (dots[x, y].GetComponent<Dot>().isMatched == true)
                    {

                        dots[x, y].GetComponent<Dot>().particles.SetActive(false);
                        dots[x, y].GetComponent<Dot>().isMatched = false;
                    }
                }
            }
        }
            


        //reset the active dots number
        currentActivePoints = 0;
        //reset the first dot clicked
        firstClickedDot = null;

        //destroy all lines
        foreach (GameObject line in lines)
        {
            Destroy(line);
        }

    }
    public void CreateDots()
    {
        //For every collumn
        for (int x = 0; x < sizeX; x++)
        {
            //In every row
            for (int y = 0; y < sizeY; y++)
            {
                //Check if it is an even row
                if (y % 2 == 0)
                {
                    //Check if that space has no dot
                    if (dots[x, y] == null)
                    {
                        //Create a new dot
                        GameObject temp = Instantiate(dotPrefab, new Vector3(spawnX - spacer / 2 + (x * spacer), spawnY + 8 + (y * spacer), 0), Quaternion.identity) as GameObject;
                        //Set the dot array to this dot
                        dots[x, y] = temp;
                        //Set the dots x,y and color values
                        temp.GetComponent<Dot>().x = x;
                        temp.GetComponent<Dot>().y = y;
                        temp.GetComponent<Dot>().color = Random.Range(0, differentColors);

                        //Lerp it down to its place
                        StartCoroutine(BallBounce(temp, temp.GetComponent<Transform>().position, new Vector3(temp.transform.position.x, temp.transform.position.y - 8, 0), 1.0f, 2.0f));
                    }
                }
                //It wasnt even so it must be an odd row
                else
                {
                    //Check if that space has no dot
                    if (dots[x, y] == null)
                    {
                        //Create a new dot
                        GameObject temp = Instantiate(dotPrefab, new Vector3(spawnX + (x * spacer), spawnY + 8 + (y * spacer), 0), Quaternion.identity) as GameObject;
                        //Set the dot array to this dot
                        dots[x, y] = temp;
                        //Set the dots x,y and color values
                        temp.GetComponent<Dot>().x = x;
                        temp.GetComponent<Dot>().y = y;
                        temp.GetComponent<Dot>().color = Random.Range(0, differentColors);

                        //Lerp it down to its place
                        StartCoroutine(BallBounce(temp, temp.GetComponent<Transform>().position, new Vector3(temp.transform.position.x, temp.transform.position.y - 8, 0), 1.0f, 2.0f));
                    }
                }


            }


        }
    }

    public void CheckDots()
    {
        bool movedDown = false;
        //For every collumn
        for (int x = 0; x < sizeX; x++)
        {
            //In every row but the bottom
            for (int y = 1; y < sizeY ; y++)
            {
                //Check if it is an even row
                if (y % 2 == 0)
                {
                    //if the current dot is not null
                    if (dots[x, y] != null)
                    { 
                        //check if the one below it is empty
                        if (dots[x, y - 1] == null)
                        {
                            //we move it down to its new place to the right
                            StartCoroutine(BallBounce(dots[x, y], dots[x, y].transform.position, new Vector3(spawnX + (x * spacer), spawnY + ((y - 1) * spacer), 0), 1.0f, 10.0f));
                            dots[x, y - 1] = dots[x, y];
                            dots[x, y - 1].GetComponent<Dot>().y -= 1;
                            dots[x, y] = null;
                            //We moved atleast one dot this loop so set movedDown to true
                            movedDown = true;
                        }
                    }
                }

                //else its an odd row
                else
                {
                    //if the current dot is not null
                    if (dots[x, y] != null)
                    {
                        //check if the one below it is empty
                        if (dots[x, y - 1] == null)
                        {
                            //we move it down to its new place to the left
                            StartCoroutine(BallBounce(dots[x, y], dots[x, y].transform.position, new Vector3(spawnX - spacer / 2 + (x * spacer), spawnY + ((y - 1) * spacer), 0), 1.0f, 10.0f));
                            dots[x, y - 1] = dots[x, y];
                            dots[x, y - 1].GetComponent<Dot>().y -= 1;
                            dots[x, y] = null;
                            //We moved atleast one dot this loop so set movedDown to true
                            movedDown = true;
                        }
                    }
                    
                }
            }
        }
        //If we never needed to move anything down the whole loop, we can say everything is moved down
        if(movedDown == false)
        {
            allMovedDown = true;
        }
    }
}
